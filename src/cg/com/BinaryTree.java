package cg.com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinaryTree {
	private Node root;
	private int maxSize;
	private Node[] nodeArray;
	private int currentSize;

	public BinaryTree(int size) {
		this.maxSize = size;
		this.nodeArray = new Node[size];
		this.root = null;
		this.currentSize = 0;
	}

	void insert(Node newNode) {
		newNode.getLeft().setDirection(Path.LEFT);
		newNode.getRight().setDirection(Path.RIGHT);
		if (currentSize == maxSize) {
			System.out.println("List full!");
			return;
		}
		if (root == null) {
			root = newNode;
			newNode.setPrev(null);
			newNode.getLeft().setPrev(root);
			newNode.getRight().setPrev(root);
			nodeArray[currentSize] = newNode;
			currentSize++;
			nodeArray[currentSize] = newNode.getLeft();
			currentSize++;
			nodeArray[currentSize] = newNode.getRight();
			currentSize++;
			return;
		}
		for (int i = 0; i < currentSize; i++) {
			Node current = nodeArray[i];
			if (current != null) {
				if (current.getKey() == newNode.getKey()) {
					newNode.setPrev(current.getPrev());
					newNode.getLeft().setPrev(current);
					newNode.getRight().setPrev(current);

					current.setLeft(newNode.getLeft());
					current.setRight(newNode.getRight());

					nodeArray[currentSize] = newNode.getLeft();
					currentSize++;
					nodeArray[currentSize] = newNode.getRight();
					currentSize++;
					return;
				}
			}
		}
	}

	Node foundNode(Node node, int key) {
		if (node == null) {
			return null;
		}
		if (node.getKey() == key) {
			return node;
		}
		Node left = foundNode(node.getLeft(), key);
		if (left != null) {
			return left;
		}
		return foundNode(node.getRight(), key);
	}

	String pathTrack(Node node) {
		List<String> paths = new ArrayList<>();
		Node tmp = foundNode(root, node.getKey());
		if (tmp != null) {
			if (root.getKey() == tmp.getKey()) {
				paths.add(Path.ROOT);
			} else {
				while (tmp.getKey() != root.getKey()) {
					paths.add(tmp.getDirection());
					tmp = tmp.getPrev();
				}
			}
		}
		Collections.reverse(paths);
		return String.join(" ", paths);
	}

	void display() {
		for (Node node : nodeArray) {
			if (node != null)
				System.out.print(node.getKey() + " ");
		}
	}

	Node getRoot() {
		return root;
	}
}
