package cg.com;

public class Node {
	private int key;
	private Node left;
	private Node right;
	private Node prev;
	private String direction;

	public Node(int key) {
		this.key = key;
	}

	@Override
	public String toString() {
		return "Node [key=" + key + "]";
	}

	int getKey() {
		return key;
	}

	void setKey(int key) {
		this.key = key;
	}

	Node getLeft() {
		return left;
	}

	void setLeft(Node left) {
		this.left = left;
	}

	Node getRight() {
		return right;
	}

	void setRight(Node right) {
		this.right = right;
	}

	String getDirection() {
		return direction;
	}

	void setDirection(String direction) {
		this.direction = direction;
	}

	Node getPrev() {
		return prev;
	}

	void setPrev(Node prev) {
		this.prev = prev;
	}

}
