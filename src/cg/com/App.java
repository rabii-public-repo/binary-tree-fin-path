package cg.com;

public class App {

	public static void main(String[] args) {
		BinaryTree bt = new BinaryTree(100);
		Node one = new Node(1);
		one.setLeft(new Node(2));
		one.setRight(new Node(3));

		Node two = new Node(2);
		two.setLeft(new Node(4));
		two.setRight(new Node(5));

		Node three = new Node(3);
		three.setLeft(new Node(9));
		three.setRight(new Node(8));
//		
		Node four = new Node(8);
		four.setLeft(new Node(6));
		four.setRight(new Node(7));

		bt.insert(one);
		bt.insert(two);
		bt.insert(three);
		bt.insert(four);
		bt.display();
		Node node = bt.foundNode(bt.getRoot(), 6);
		// 5 faux right left -> left right
		// 9 faux left right -> right left
		// 6 faux left right right -> right right left

		System.out.println(node);
		System.out.println(bt.pathTrack(node));
	}

}
